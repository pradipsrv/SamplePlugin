// Empty constructor
function samplePlugin() {}

// The function that passes work along to native shells
// Message is a string, duration may be 'long' or 'short'
samplePlugin.prototype.show = function(message, duration, successCallback, errorCallback) {
  var options = {};
  options.message = message;
  options.duration = duration;
  cordova.exec(successCallback, errorCallback, 'samplePlugin', 'show', [options]);
}


samplePlugin.prototype.EasebuzzPay = function(options, successCallback, errorCallback) {
        console.log('**************params***inPlugin****');
        console.log(JSON.stringify(options));

  		  cordova.exec(successCallback, errorCallback, 'samplePlugin', 'paywitheasebuzz', [
          JSON.stringify(options)
        ]);

}

// Installation constructor that binds ToastyPlugin to window
samplePlugin.install = function() {
  if (!window.plugins) {
    window.plugins = {};
  }
  window.plugins.samplePlugin = new samplePlugin();
  return window.plugins.samplePlugin;
};
cordova.addConstructor(samplePlugin.install);